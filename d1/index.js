//console.log("Hello World");

//Functions

	//Parameters and Arguments

		// function printInput (){
		// 	let nickName = prompt("Enter your nickname:");
		// 	console.log("Hi, " + nickName);
		// };
		// printInput();

		//for some other cases, functions can also process data directly passed into it instead of relying only on global variables and prompt()
		
		function printName(name){ //parameter
			console.log("My name is " + name);
		};

		printName("Juana"); //argument
		
		//you can directly pass data into the function
		//the function can then call/use that data which is referred as "name" within the function
		//"name" is called a parameter
		//a "parameter" acts as a named variable/container that exists only inside of a function
		//it is used to store info that is provided to a function when it os called or invoked
		//"Juana" the info/data provided directly into a function is called an argument

		printName("John"); //John and Jane are both arguments
		printName("Jane");

		//variables can also be passed as an argument
		let sampleVariable = "Yui"; 
		printName(sampleVariable);

		//Function arguments cannot be used a s a function if there are no parameters provided within the function.

		function checkDivisibilityBy8(num){
			let remainder = num % 8;
			console.log("The remainder of " + num + " divided by 8 is " + remainder);
			let isDivisibleBy8 = remainder === 0;
			console.log("Is " + num + " divisible by 8?");
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8(64);
		checkDivisibilityBy8(28);

		function checkDivisibilityBy4(num){
			let remainder = num % 4;
			console.log("The remainder of " + num + " divided by 4 is " + remainder);
			let isDivisibleBy4 = remainder === 0;
			console.log("Is " + num + " divisible by 4?");
			console.log(isDivisibleBy4);
		};

		checkDivisibilityBy4(56);
		checkDivisibilityBy4(95);

		//you can also the same using our prompt(), however take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

		//Functions as Arguments
		//Function parameters can also accept other functions as arguments
		//Some complex functions used some other functions as arguments to perform more complicated result

		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed");
		};

		function invokeFunction(argumentFunction) {
			argumentFunction();
		};

		invokeFunction(argumentFunction);
		console.log(argumentFunction);


		function createFullName(firstName, middleName, lastName){
			console.log(firstName + ' ' + middleName + ' ' + lastName);
		};
		createFullName("Juan", "Dela", "Cruz");

		//mini-activity
		function printFriends(friend1, friend2, friend3){
			console.log("My three friends are: " + friend1 + ' ' + friend2 + ' ' + friend3);
		};
		printFriends("Mark", "Joyce", "Teresa");

		createFullName("Juan", "dela");
		createFullName("Jane", "dela", "Cruz", "Hello");

		//we can use variables as arguments
		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName, middleName, lastName);

		function printFullName(middleName, firstName, lastName){
			console.log(firstName + ' ' + middleName + ' ' + lastName);
		}
		printFullName("Juan","Dela","Cruz");

		//Return Statement

		//allows us to output a value from a function to be passed to the line or block of code that is either invoked or called the function
		
		function returnFullName(firstName, middleName, lastName) {
			//console.log(firstName + " " + middleName + " " + lastName);
			return firstName + ' ' + middleName + ' ' + lastName;
			console.log("This message will not be printed");
		};

		let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
		console.log(completeName);

		let completeName2 = returnFullName("Nehemiah", "C.", "Ellorico");
		console.log(completeName2);

		let combination = completeName + completeName2;
		console.log(combination);

		console.log(returnFullName(firstName, middleName, lastName));

		function returnAddress(city, country){
			let fullAddress = city + ", " + country;
			return fullAddress;
		}

		let myAddress = returnAddress("Cebu City", "Philippines");
		console.log(myAddress);

		function printPlayerInfo(UserName, level, job){
			/*console.log("Username: " + UserName);
			console.log("Level: " + level);
			console.log("Job: " + job);*/
			return("Username: " + UserName + " Level: " + level + " Job: " + job);
		};
		let user1 = printPlayerInfo("knight_white", 95, "Paladin");
		console.log(user1); //undefined without return

		//Mini-activity

		function printProduct(num1, num2) {
			console.log("Displayed product of " + num1 + " and " + num2);
			return num1 * num2;
		};

		let product = printProduct(21, 10);
		console.log(product);

